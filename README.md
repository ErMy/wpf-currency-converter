# Currency Converter
An app for converting currency using current exchange rates from openexchangerates. Made with Windows Presentation Foundation (WPF).

[Get the release here](https://drive.google.com/file/d/1xPJAhryX1Nvu3HF1q2-8v22kI7KEMaIC/view?usp=sharing)
