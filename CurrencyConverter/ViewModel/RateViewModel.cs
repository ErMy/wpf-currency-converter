﻿using CurrencyConverter.Helper;
using CurrencyConverter.Model;
using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Windows.Input;

namespace CurrencyConverter.ViewModel
{
    /// <summary>
    /// Logic for converting currency and creating the rates data
    /// </summary>
    public class RateViewModel : INotifyPropertyChanged
    {
        #region Variables
        /// <summary>
        /// Swap <see cref="FromIndex"/> and <see cref="TooIndex"/>
        /// </summary>
        public ICommand SwapCommand { get; set; }
        /// <summary>
        /// Event that fires when one of its properties changes
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private DataTable _rateTable;
        /// <summary>
        /// Contains names and rates retrieved online
        /// </summary>
        public DataTable RateTable
        {
            get
            {
                return _rateTable;
            }
            set
            {
                _rateTable = value;
                OnPropertyChanged("RateTable");
            }
        }

        private string _amount;
        /// <summary>
        /// Amount of currency. Connected to the amount textbox
        /// </summary>
        public string Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
                ConvertCurrency();
                OnPropertyChanged("Amount");
            }
        }

        private int _fromIndex;
        /// <summary>
        /// Corresponds to a row in the combobox/datatable. Connected to the FromCombobox
        /// </summary>
        public int FromIndex
        {
            get
            {
                return _fromIndex;
            }
            set
            {
                _fromIndex = value;
                ConvertCurrency();
                Properties.Settings.Default.FromIndex = value;
                OnPropertyChanged("FromIndex");
            }
        }

        private int _toIndex;
        /// <summary>
        /// Corresponds to a row in the combobox/datatable. Connected to the ToCombobox
        /// </summary>
        public int ToIndex
        {
            get
            {
                return _toIndex;
            }
            set
            {
                _toIndex = value;
                ConvertCurrency();
                Properties.Settings.Default.ToIndex = value;
                OnPropertyChanged("ToIndex");
            }
        }

        private string _conversionResult;
        /// <summary>
        /// Result of the conversion. Connected to the textblock showing the result.
        /// </summary>
        public string ConversionResult
        {
            get
            {
                return _conversionResult;
            }
            set
            {
                _conversionResult = value;
                OnPropertyChanged("ConversionResult");
            }
        }
        #endregion

        #region Constructor
        public RateViewModel()
        {
            SwapCommand = new RelayCommand(SwapFromTo);
            InitializeValues();
        }
        #endregion

        #region View Model Logic
        /// <summary>
        /// Retrieve rates from online source and create datatable.
        /// Set properties to their saved or default values.
        /// </summary>
        private async void InitializeValues()
        {
            Rates rates = await RatesDownloader.GetRates();

            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Rate");

            foreach (PropertyInfo info in rates.rates.GetPropertyInfos())
            {
                dt.Rows.Add(info.Name, info.GetValue(rates.rates));
            }

            RateTable = dt;
            FromIndex = Properties.Settings.Default.FromIndex;
            ToIndex = Properties.Settings.Default.ToIndex;
            Amount = "0";
        }

        public double ConvertCurrency()
        {
            double result = 0;
            if (double.TryParse(Amount, out double fromAmount))
            {
                try
                {
                    if (double.TryParse(RateTable.Rows[FromIndex]["Rate"].ToString(), out double fromRate)
                        && double.TryParse(RateTable.Rows[ToIndex]["Rate"].ToString(), out double toRate))
                    {
                        if (fromRate != 0 && toRate != 0)
                        {
                            result = fromAmount / fromRate * toRate;
                            ConversionResult = $"{(Math.Round(result, 2)).ToString("0.##")} {Convert.ToString(RateTable.Rows[ToIndex]["Name"])}";
                        }
                    }
                } catch
                {
                    ConversionResult = "";
                }
            } else
            {
                ConversionResult = "";
            }

            return result;
        }

        public void SwapFromTo()
        {
            int temp = FromIndex;
            FromIndex = ToIndex;
            ToIndex = temp;
        }
        #endregion

        #region Interface
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
