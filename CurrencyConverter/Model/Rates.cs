﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Model
{
    /// <summary>
    /// Container for <see cref="RateData"/>. Save rates JSON data in this class
    /// </summary>
    public class Rates
    {
        public RateData rates { get; set; }
    }

    public class RateData
    {
        public double AUD { get; set; }
        public double CAD { get; set; }
        public double CHF { get; set; }
        public double EUR { get; set; }
        public double GBP { get; set; }
        public double HKD { get; set; }
        public double JPY { get; set; }
        public double NZD { get; set; }
        public double SEK { get; set; }
        public double USD { get; set; }
        public double ZAR { get; set; }

        public PropertyInfo[] GetPropertyInfos()
        {
            return GetType().GetProperties();
        }
    }
}
