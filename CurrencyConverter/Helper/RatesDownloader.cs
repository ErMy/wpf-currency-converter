﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using CurrencyConverter.Model;

namespace CurrencyConverter.Helper
{
    /// <summary>
    /// Get the current rates from openexchangerates.org
    /// </summary>
    public class RatesDownloader
    {
        const string APILINK = "https://openexchangerates.org/api/latest.json?app_id=69cb235f2fe74f03baeec270066587cf";

        public static async Task<Rates> GetRates()
        {
            Rates rates = new Rates();
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(1);

                    HttpResponseMessage response = await client.GetAsync(APILINK);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var ResponseString = await response.Content.ReadAsStringAsync();

                        var ResponseObject = JsonConvert.DeserializeObject<Rates>(ResponseString);
                        return ResponseObject;
                    }
                    return rates;
                }
            }
            catch
            {
                return rates;
            }
        }
    }
}
