﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using CurrencyConverter.Helper;
using CurrencyConverter.ViewModel;
using System.Data;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestRatesDownload()
        {
            var rates = RatesDownloader.GetRates();
            Assert.IsNotNull(rates);
        }

        [TestMethod]
        public void TestConversionNormal()
        {
            var viewModel = new RateViewModel();
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Rate");

            dt.Rows.Add("EUR", "1.0");
            dt.Rows.Add("USD", "0.9");
            viewModel.RateTable = dt;
            
            viewModel.Amount = "100";
            viewModel.FromIndex = 0;
            viewModel.ToIndex = 1;

            Assert.AreEqual(100 * 0.9, viewModel.ConvertCurrency(), 0.01);
        }

        [TestMethod]
        public void TestConversionNegativeRates()
        {
            var viewModel = new RateViewModel();
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Rate");

            dt.Rows.Add("EUR", "-1.0");
            dt.Rows.Add("USD", "-0.9");
            viewModel.RateTable = dt;

            viewModel.Amount = "100";
            viewModel.FromIndex = 0;
            viewModel.ToIndex = 1;

            Assert.AreEqual(100 * 0.9, viewModel.ConvertCurrency(), 0.01);
        }

        [TestMethod]
        public void TestConversionZeroAmount()
        {
            var viewModel = new RateViewModel();
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Rate");

            dt.Rows.Add("EUR", "1.0");
            dt.Rows.Add("USD", "0.9");
            viewModel.RateTable = dt;

            viewModel.Amount = "0";
            viewModel.FromIndex = 0;
            viewModel.ToIndex = 1;

            Assert.AreEqual(0, viewModel.ConvertCurrency(), 0.01);
        }

        [TestMethod]
        public void TestConversionEmptyAmount()
        {
            var viewModel = new RateViewModel();
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Rate");

            dt.Rows.Add("EUR", "1.0");
            dt.Rows.Add("USD", "0.9");
            viewModel.RateTable = dt;

            viewModel.Amount = "";
            viewModel.FromIndex = 0;
            viewModel.ToIndex = 1;

            Assert.AreEqual(0, viewModel.ConvertCurrency(), 0.01);
        }

        [TestMethod]
        public void TestConversionEmptyRates()
        {
            var viewModel = new RateViewModel();
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Rate");

            dt.Rows.Add("EUR", "");
            dt.Rows.Add("USD", "");
            viewModel.RateTable = dt;

            viewModel.Amount = "100";
            viewModel.FromIndex = 0;
            viewModel.ToIndex = 1;

            Assert.AreEqual(0, viewModel.ConvertCurrency(), 0.01);
        }

        [TestMethod]
        public void TestConversionZeroRates()
        {
            var viewModel = new RateViewModel();
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Rate");

            dt.Rows.Add("EUR", "0");
            dt.Rows.Add("USD", "0");
            viewModel.RateTable = dt;

            viewModel.Amount = "100";
            viewModel.FromIndex = 0;
            viewModel.ToIndex = 1;

            Assert.AreEqual(0, viewModel.ConvertCurrency(), 0.01);
        }
    }
}
